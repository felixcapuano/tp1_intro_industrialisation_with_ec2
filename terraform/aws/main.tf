terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.11"
        }
    }
}

provider "aws" {
  region = "eu-west-1"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key-esme"
  //public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDpvAqvLmf2lNwQlNrz7qhq9MzMjUFyxIM0unhu7qDqcw2cNI81hQO+UvG80IlRj1xw2qCuytaLmkbNrfvi/5dXOAvyMCn6HGj6NuszxG5xVJtrimGhCuKl9JPc5jLSEG2vMhJTL/eJkJJS1M+HaU7RsVqGEKHIojiLhKA6uesjehRXyCjmUTZoXpmeQW5IzNQ5ZXgiMWkgJbTtt3svU9QCFi6Ov/inAhrApDX8+Gw40Xu890Jn7FeLux53Sn92cfdC/YE0kVPC+vI9g/rBqyzwpbTmylCD92BeLYo4LKwnS3HD4clo4zh7q70vRWpw3kfaR2poizIrZMkFYsMsWXDVFs9tmbmnrCGUTWWZtupWOb4ZWKt//IW9+G5sF0MDockQptxS3Uj0u4TmEzUH8GDr56WYm7ngjq2DxLAimj35ZzNoHaLPWM1CAIW3X7GggYqXE+izG2ug8D3OxAE4hDZo30rTJuXF7oFHEiGqYvVxGGKzeFg3pvSvPSVbEOBv+1U= ubuntu@ubuntu"
  public_key = file(var.aws_public_key_ssh_path)
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  =  6
    self      = true
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = 6
    self      = true
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = var.tag_name
  }
 
}